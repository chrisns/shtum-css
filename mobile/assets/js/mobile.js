// Add has-js class
document.documentElement.className = document.documentElement.className.replace('no-js', 'js');

/* Styled checkbox & radio ***/
function styleCheckboxRadio() {
	if ($('.checkbox input').length) {
		if(!$('.checkbox .check').length)
		{
			$('<span class="check"><span /></span>').prependTo($('.checkbox'));
		}
			
		$('.checkbox').removeClass('c-on');
		$('.check').removeClass('checked');

		$('.checkbox input:checked').closest('label').addClass('c-on');
		$('.c-on .check').addClass('checked');	
	};
	
	if ($('.radio input').length) {
		if(!$('.radio .tick').length)
		{
			$('<span class="tick"><span /></span>').prependTo($('.radio'));
		}

		$('.radio').removeClass('r-on');
		$('.tick').removeClass('ticked');
		$('.radio input:checked').closest('label').addClass('r-on');
		$('.r-on .tick').addClass('ticked');
	};
};

/* Styled input[type="file"] ***/
function styleInputFile(){
	if ($('input[type="file"]').length) {
		$('input[type="file"]').each(function(){
			$(this)
				.wrap('<div class="file-upload" />')
				.after('<a href="#" class="button add">' + $(this).attr('placeholder') + '</a>');
		});
	};
};

jQuery(function($){ 

	// Fancy form elements - on load
	styleCheckboxRadio();
	styleInputFile();

	// Fancy form elements - on click
	$('.checkbox, .radio, .selectbox').click(function(){
		styleCheckboxRadio();

		// Show actions
		if($('.c-on').hasClass('show-action'))
		{
			var $target = $(this).parent().attr('class'),
				$actionsClass = $target == undefined ? 'mark-unread' : 'mark-read';
			
			$('.title .actions').toggleClass($actionsClass).toggleClass('open');
		}
	});

	// Show password toggle
	$('.has-password').each(function(){
		if ($(this).children('.toggle-password').text() == 'Mask')
		{
			$(this).find('input.is-password').attr({'type':'text'});
		}

		$(this).children('.toggle-password').click(function(){
			$(this)
				.text($(this).text() == 'Unmask' ? 'Mask' : 'Unmask')
				.prev().children('input.is-password').attr('type', $(this).prev().children('input.is-password').attr('type') == 'text' ? 'password' : 'text');
		});
	});

	// Select on focus
	$('input.permalink, .permalink input').focus(function() {
	   $(this).select();
	});

	// Detect nested replies
	$('.threads li:not(.opening)').each(function(){
	    if ($(this).children('ol').length > 0){
	        $(this).addClass('has-replies')
	    }
	});
	
	// Enable sign up button	
	$('.enable-action').click(function(){
		$('#signup').attr('disabled', !this.checked);
	});

	// Active icons if panel is open
	$('.ui-panel-position-left')
		.on('panelbeforeopen', function(event,ui){
			$('.icon.menu').addClass('active');
		}).on('panelbeforeclose', function(event,ui){
			$('.icon.menu').removeClass('active');
		});
	$('.ui-panel-position-right')
		.on('panelbeforeopen', function(event,ui){
			$('.icon.profile').addClass('active');
		}).on('panelbeforeclose', function(event,ui){
			$('.icon.profile').removeClass('active');
		});

	// Board actions
	$('.title .show-action, .title .hide-action').click(function(){
	 	$(this).toggleClass('hide-action').toggleClass('show-action');
	 	$('.title .actions').toggleClass('open');
    });

    // Show hidden boards
    $('.panel .show-hidden').click(function(){
	 	$(this).prev('.panel-links').toggleClass('hidden');
	 	$(this).toggleClass('open');
	 	if ($(this).hasClass('open'))
			$(this).text('Hide hidden boards').show();
		else
			$(this).text('Show hidden boards').show();
	});

    // Show popup options for post
	$('.post[data-rel="popup"]').click(function(){
		var $target = $(this).attr('id');
		
		$(this).addClass('active');
		$('<a href="#" data-rel="back" class="ui-btn-right">Close</a>').prependTo($('#' + $target + '-popup'));
		
		$('#' + $target + '-popup').popup('open').bind({
			popupafterclose: function(event, ui) { 
				$(this).find('.ui-btn-right').remove();
				$('#' + $target).removeClass('active');
   			}
		});

	});

	// Prevent event if inside link is clicked
	$('.post[data-rel="popup"] a').click(function(e){
		e.stopPropagation();
	});

	// Expand/collapse post replies
	$('.collapsible').click(function(){
		var $target = $(this).parent('.post-actions').attr('id').split('-');
		
		$('#' + $target[0]).parent('li').toggleClass('collapsed').find('ol').animate({height: 'toggle'});
		
		if ($('#' + $target[0]).parent('li').hasClass('collapsed'))
			$(this).text('Expand').show();
		else
			$(this).text('Collapse').show();
	});

	// Style tags
	$('.tags')
		.textext({plugins:'tags'})
		.focusout(function(){
			var $values = $('input[name=' + $(this).attr('id') + ']').val(),
				$placeholder = $(this).attr('placeholder');
			if ($values != '[]') $(this).removeAttr('placeholder');
		});

	// Show photo and video inputs on click
	$('.entry-form .actions .button').click(function(){
		var $target = $(this).attr('href'),
			$type = $(this).attr('class').split(' ');

		$($target).toggleClass('hidden');
		$(this).toggleClass('open');

		if ($(this).hasClass('open'))
		{
			$(this).text('Remove ' + $type[1]).show();
		}
		else
		{
			$(this).text('Add ' + $type[1]).show();
			$('#add-' + $type[1] + ' input').val('');
		}
	});

	// Check if value is entered before enabling buttons
	/*$('.check-data .button.add').addClass('disabled');
	$('.check-data .required input, .check-data .required textarea').keyup(function() {

        var empty = false;
        $('.check-data .required input, .check-data .required textarea').each(function() {
            if ($(this).val() == '') {
                empty = true;
            }
        });

        if (empty) {
            $('#step-1 .button.add').addClass('disabled');

        } else {
            $('#step-1 .button.add').removeClass('disabled'); 
        }
    });*/

});