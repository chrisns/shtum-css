module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    sass: {
      dist: {
        files: {
          'desktop/assets/css/layout.desktop.css' : 'sass/layout.desktop.scss',
          'mobile/assets/css/layout.mobile.css' : 'sass/layout.mobile.scss',
          'desktop/assets/css/type.css' : 'sass/type.scss',
          'mobile/assets/css/type.css' : 'sass/type.scss',
          'mobile/assets/css/jquery.mobile.css' : 'sass/jquery.mobile.scss'
        }
      }
    },
    watch: {
      css: {
        files: '**/*.scss',
        tasks: ['sass']
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('default',['sass', 'watch']);
}