// Panels height
function equalizeHeight(){
    if ($('#content-wrapper').height() >= $(window).height())
    {
        $('.panel').attr({'style': 'height:' + $('#content-wrapper').height() + 'px;'});
    }
    else 
    {
        $('.panel').attr({'style': 'height:' + $(window).height() + 'px;'});
    }
}

// Disable scrolling
function disableMiddleMouseButtonScrolling(e){
    if(e.which == 2)
    {
        e.preventDefault();
    }
    return false;
}
function disableNormalScroll(e){
    e.preventDefault();
    $('html, body').scrollTop(oldScrollTop);
    return false;
}

// Close inline forms
function closeInlineForm(){
	if($('.inserted-form').length){
    	$('.inserted-form').slideUp('slow', function() { 
    		$(this).remove(); 
    		$('.threads li .post').each(function(){
	        	$(this).removeAttr('style');
	        });
    	});
    }
}

// Scroll to new posts
function scroll(direction){
	var scroll, i, j,
		positions = [],
		starredPositions = [],
		here = $(window).scrollTop(),
		collection = $('.post.new'),
		starredCollection = $('.pinned .post.new');

	// All new posts
	collection.each(function() {
		positions.push(parseInt($(this).offset()['top'],10));
	});

	for(i = 0; i < positions.length; i++) {
		if ((direction == 'next' || direction == 'next-top') && positions[i] > here) { scroll = collection.get(i); break; }
		if (direction == 'prev' && i > 0 && positions[i] >= here) { scroll = collection.get(i-1); break; }
	}

	// Only pinned new posts
	starredCollection.each(function() {
		starredPositions.push(parseInt($(this).offset()['top'],10));
	});

	for(j = 0; j < starredPositions.length; j++) {	
		if ((direction == 'next-pinned' || direction == 'next-pinned-top') && starredPositions[j] > here) { scroll = starredCollection.get(j); break; }
		if (direction == 'prev-pinned' && j > 0 && starredPositions[j] >= here) { scroll = starredCollection.get(j-1); break; }
	}

	if (scroll) {
		$.scrollTo(scroll, {
			duration: 	150,
			axis:     	'y'    
		});
	}

	return false;	
}

// Cache inline form content
$.ajaxSetup({
  cache: true
});

jQuery(function($){ 

	// Check if layout should be cozy - DEMO
	var $uri = window.location.href.split('?');
	if ($uri[1] == 'style=cozy')
		$('body').addClass('cozy');
		
	// Remove new posts notification
	$('#new-posts .close, #nav-dock .close').click(function(){
		$(this).parent('div').remove();
	})

	// Nav dock
    $('#nav-dock').hide(); // Hide at first
    $('#nav-dock a').click(function() {    
        return scroll($(this).attr('id'));
    }); 
	
	// Scroll to new posts from the top
    $('#new-posts a').click(function() { 
    	// Close panel
	    $('#content-wrapper').removeClass('active-left').removeClass('active-right');
	    $('.panel-link').removeClass('active');

    	$('#nav-dock').show();	// Show nav dock 
        return scroll($(this).attr('id'));
    });

	// Show extra options when over title
	$('#thread-actions').hover(function(){
		$('#thread-actions ul').toggle();
	});

	// Detect nested replies
	$('.threads li:not(.opening)').each(function(){
	    if ($(this).children('ol').length > 0){
	        $(this).addClass('has-replies')
	    }
	});

	// Expand/collapse post replies
	$('.collapsible').click(function(e){
		e.preventDefault();
		
		var $target = $(this).parent().parent().attr('id');

		$('#' + $target).toggleClass('collapsed').find('ol').animate({height: 'toggle'});
		
		if ($('#' + $target).hasClass('collapsed'))
			$(this).attr({'title':'Expand'}).text('+').show();
		else
			$(this).attr({'title':'Collapse'}).text('-').show();

		// Clear panel height
		$('#main-nav').removeAttr('style');
	});

	// Show hidden boards
    $('.panel .show-hidden').click(function(){
	 	$(this).prev('.panel-links').toggleClass('hidden');
	 	$(this).toggleClass('open');
	 	if ($(this).hasClass('open'))
			$(this).text('Hide hidden boards').show();
		else
			$(this).text('Show hidden boards').show();
	});

	// Tooltip
	$('.tooltip')
		.hover(function(){
	        // Hover over code
	        var title = $(this).attr('title');
	        $(this).data('tipText', title).removeAttr('title');
	        $('<p class="tip"></p>')
	        .text(title)
	        .appendTo('body')
	        .fadeIn('slow');
	        $('<span class="arrow" />').appendTo('.tip');
		}, function() {
		        // Hover out code
		        $(this).attr('title', $(this).data('tipText'));
		        $('.tip').remove();
		}).mousemove(function(e) {
		        var mousex = e.pageX - $('.tip').width(); //Get X coordinates
		        var mousey = e.pageY + 20; //Get Y coordinates
		        $('.tip')
		        .css({ top: mousey, left: mousex })
		});

	// Show message body
	$('.messages-list:not(.trash) .listing-head').click(function(){
		$(this).parent('li').toggleClass('opened');
		$(this).next('.conversation').toggle('slow');
	});

	// Star conversations
	$('.star').click(function(){
		$(this).toggleClass('starred');
		$(this).closest('li').toggleClass('pinned');
	});

	// Add inline form into page
	$('a[data-rel="add-form"]').click(function(e){
		e.preventDefault();
		var $target = $(this).closest('li').attr('id'),
			$type = $(this).attr('class'),
			$data = $(this).attr('href');

		switch($type){
			case "reply":
				if (!$('.reply-form').length)
					$('<div id="' + $target + '-form" class="inserted-form reply-form" />').insertAfter($(this).closest('.post'));
				if ($('.inserted-form:not(.reply-form)').length)
					$('.inserted-form:not(.reply-form)').slideUp('slow', function() {
						$(this).remove();
						$('.threads li').each(function(){
							$(this).removeClass('disabled').removeClass('focus');
				        });
		  			});
				break; 
            case "edit":
            	if (!$('.edit-form').length)
					$('<div id="' + $target + '-form" class="inserted-form edit-form" />').insertAfter($(this).closest('.post'));
				if ($('.inserted-form:not(.edit-form)').length)
					$('.inserted-form:not(.edit-form)').slideUp('slow', function() {
						$(this).remove();
						$('.threads li').each(function(){
							$(this).removeClass('disabled').removeClass('focus');
				        });
		  			});
            	break;
            default:
            	return false;
            break;
		}

		$(this).addClass('loading');
		$('#' + $target + '-form').load($data,function(){
	        $('a.loading[data-rel="add-form"]').removeClass('loading');
	        $(this).slideDown('slow');
	        
	        $(this).closest('li').addClass('focus');
	        $('.threads li:not(.focus)').addClass('disabled');

	    });

		// Close panel
	    $('#content-wrapper').removeClass('active-left').removeClass('active-right');
	    $('.panel-link').removeClass('active');
	});

	// Panels
    var $menulink = $('.panel-link'),
        $wrap = $('#content-wrapper');
    
	    $menulink.click(function(e) {
	    	e.preventDefault();
	        equalizeHeight();

	        // Close inline forms
	        closeInlineForm();

			var $target = $(this).attr('href'),
				$actionsClass = $target == '#main-nav' ? 'active-left' : 'active-right';
			$(this).toggleClass('active');
			$wrap.toggleClass($actionsClass);
	    });

    // Fancybox - data-rel dialog for outer pages
	$('a[data-rel="dialog"]').click(function(e){
		e.preventDefault();

		// Remove opened forms, clear styling, close panel
		$('#content-wrapper').removeClass('active-left').removeClass('active-right');
		$('.panel-link').removeClass('active');
		 closeInlineForm();
	}).fancybox({
		padding			: '0',
		width			: '960',
		height 			: '95%',
		autoSize 		: true,
		autoScale		: true,

		type 			: 'iframe',
		iframe:
		{
			scrolling 	: 'no',
			preload   	: false
		},
		arrows			: false,
		helpers:
		{
			title		: null
			
		}
	});

	$('.modal-close').click(function(e){
    	parent.$.fancybox.close(true)
	});

});