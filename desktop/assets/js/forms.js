// Toggle switch
jQuery.fn.toggleSwitch = function (params) {

    var defaults = {
        highlight: true,
        width: 25,
        change: null
    };

    var options = $.extend({}, defaults, params);

    $(this).each(function (i, item) {
        generateToggle(item);
    });

    function generateToggle(selectObj) {

        // create containing element
        var $contain = $("<div />").addClass("ui-toggle-switch");

        // generate labels
        $(selectObj).find("option").each(function (i, item) {
            $contain.append("<label class='toggle'>" + $(item).text() + "</label>");
        }).end().addClass("ui-toggle-switch");

        // generate slider with established options
        var $slider = $("<div />").slider({
            min: 0,
            max: 100,
            animate: "fast",
            change: options.change,
            stop: function (e, ui) {
                var roundedVal = Math.round(ui.value / 100);
                var self = this;
                window.setTimeout(function () {
                    toggleValue(self.parentNode, roundedVal);
                }, 11);
            },
            range: (options.highlight && !$(selectObj).data("hideHighlight")) ? "max" : null
        }).width(options.width);

        // put slider in the middle
        $slider.insertAfter(
            $contain.children().eq(0)
		);

        // bind interaction
        $contain.delegate("label", "click", function () {
            if ($(this).hasClass("ui-state-active")) {
                return;
            }
            var labelIndex = ($(this).is(":first-child")) ? 0 : 1;
            toggleValue(this.parentNode, labelIndex);
        });

        function toggleValue(slideContain, index) {
            $(slideContain).find("label").eq(index).addClass("ui-state-active").siblings("label").removeClass("ui-state-active");
            $(slideContain).parent().find("option").eq(index).attr("selected", true);
            $(slideContain).find(".ui-slider").slider("value", index * 100);
        }

        // initialise selected option
        $contain.find("label").eq(selectObj.selectedIndex).click();

        // add to DOM
        $(selectObj).parent().append($contain);

    }
};

// Styled checkbox & radio
function styleCheckboxRadio() {
	if ($('.checkbox input').length) {
		if(!$('.checkbox .check').length)
		{
			$('<span class="check"><span /></span>').prependTo($('.checkbox'));
		}
			
		$('.checkbox').removeClass('c-on');
		$('.check').removeClass('checked');

		$('.checkbox input:checked').closest('label').addClass('c-on').parent('li').toggleClass('selected');
		$('.c-on .check').addClass('checked');	
	};
	
	if ($('.radio input').length) {
		if(!$('.radio .tick').length)
		{
			$('<span class="tick"><span /></span>').prependTo($('.radio'));
		}

		$('.radio').removeClass('r-on');
		$('.tick').removeClass('ticked');
		$('.radio input:checked').closest('label').addClass('r-on');
		$('.r-on .tick').addClass('ticked');
	};
};

// Styled input[type="file"]
function styleInputFile(){
	if ($('input[type="file"]').length) {
		$('input[type="file"]').each(function(){
			$(this)
				.wrap('<div class="file-upload" />')
				.after('<a href="#" class="button add">' + $(this).attr('data-title') + '</a>');
		});
	};
};

// Captcha validation
$.validator.methods.equal = function(value, element, param) { 
	return value == param;
};

// Step form validation
$.validator.addMethod("pageRequired", function(value, element) {
	var $element = $(element)
	function match(index) {
		return current == index && $(element).parents("#step-" + (index + 1)).length;
	}
	if (match(0) || match(1) || match(2)) {
		return !this.optional(element);
	}
	return "dependency-mismatch";
}, $.validator.messages.required)

jQuery(function($){ 

	// Functions onLoad
	styleCheckboxRadio();
	styleInputFile();
	$('.toggle-switch').toggleSwitch();

	// Fancy form elements - on click
	$('.checkbox, .radio, .selectbox').click(function(){
		styleCheckboxRadio();
	});

	// Toogle login forms
	$('.change-form').click(function(e){
		$('#login-signup').toggleClass('show-login');
		e.preventDefault();
	});

	// Enable sign up button	
	$('.enable-action').click(function(){
		$('#signup').attr('disabled', !this.checked);
	});

	// Show password toggle
	$('.has-password').each(function(){
		if ($(this).children('.toggle-password').text() == 'Mask')
		{
			$(this).find('input[data-rel="is-password"]').attr({'type':'text'});
		}

		$(this).children('.toggle-password').click(function(){
			$(this)
				.text($(this).text() == 'Unmask' ? 'Mask' : 'Unmask')
				.prevAll('input[data-rel="is-password"]').attr('type', $(this).prevAll('input[data-rel="is-password"]').attr('type') == 'text' ? 'password' : 'text');
		});
	});

	// Select on focus
	$('input.permalink, .permalink input').focus(function() {
	   $(this).select();
	});

	// Inline form button actions
	$('.entry-form .actions .button:not(.add):not(.simplemodal-close)').click(function(e){
		e.preventDefault();

		if ($(this).attr('data-rel') == 'remove-form')
		{
			$(this).closest('.inserted-form').slideUp('slow', function() {
				$('.threads li').each(function(){
					$(this).removeClass('disabled').removeClass('focus');
		        });
				$(this).remove();
  			});
		}
		else
		{
			var $target = $(this).attr('href'),
			$type = $(this).attr('class').split(' ');

			$($target).toggleClass('hidden');
			$(this).toggleClass('open');

			if ($(this).hasClass('open'))
			{
				$(this).text('Remove ' + $type[1]).show();
				if($type[1] == 'photo')
					$('#add-photo .thumbnail').remove();
			}
			else
			{
				$(this).text('Add ' + $type[1]).show();
				$('#add-' + $type[1] + ' input').val('');
			}
		}
	});

	// Photo zoom
	$('.thumbnail')
		.hover(function(){
	        // Hover over code
	        var href = $(this).attr('href');
	        $(this).data('tipText', href).removeAttr('href');
	        $('<img src="' + href + '" class="img-zoom" />')
	        	.appendTo('body')
	        	.fadeIn('slow');
		}, function() {
		        // Hover out code
		        $(this).attr('href', $(this).data('tipText'));
		        $('.img-zoom').remove();
		}).mousemove(function(e) {
		        var mousex = e.pageX + 20; //Get X coordinates
		        var mousey = e.pageY + 10; //Get Y coordinates
		        $('.img-zoom')
		        .css({ top: mousey, left: mousex })
		});

	// Style tags
	$('.tags')
		.textext({plugins:'tags'})
		.focusout(function(){
			var $values = $('input[name=' + $(this).attr('id') + ']').val(),
				$placeholder = $(this).attr('placeholder');
			if ($values != '[]') $(this).removeAttr('placeholder');
		});

	// Autocomoplete - members
	$.ajax({
	    url: "http://www.perceptiveinteractive.com/shtum/desktop/members.xml",
	    dataType: "xml",
	    success: function(xmlResponse) {
	         var data = $("item", xmlResponse).map(function() {
	         return {
	             value: $("value", this).text(),
	             id: $("id", this).text()
	         };
	         }).get();

	         $('#new-receiver.autocomplete').autocomplete({
	             source: data
	         }).change(function(){
	         	if($(this).hasClass('valid'))
	         	{
	         		$('.ui-helper-hidden-accessible').toggleClass('ui-helper-hidden');
	         	}
	         });
	     }
	});

	// Form validation defaults
	jQuery.validator.setDefaults({
  		errorElement    : 'em',
        errorClass      : 'note error',
        rules: {
        	name: 		{ required: true },
        	board: 		{ required: true },
        	email: 		{ required: true, email: true},
            password1: 	{ required: true, minlength: 7 },
            password2: 	{ required: true, minlength: 7, equalTo:'#signup-password' },
            captcha: 	{ equal: 4 },

            header: 	{ required: true },
            body: 		{ required: true },

            receiver: 	{ required: true },
            subject: 	{ required: true },
            message:    { required: true },

            new_password1: 	{ minlength: 7 },
            new_password2: 	{ minlength: 7, equalTo:'#account-password' }
        },
        
        // Specify the validation error messages
        messages: {
            name: 				"Please provide your full name",
            board: 				"Please provide a board name",
            email: 				"Please enter a valid email address",
            password1: {
                required: 		"Please provide a password",
                minlength: 		"Your password must be at least {0} characters long"
            },
            password2:{
                required: 		"Please provide a password",
                minlength: 		"Your password must be at least {0} characters long",
                equalTo: 		"Passwords do not match!"
            },
            captcha: 			"Please provide the correct answer",

            header: 			"Please enter heading",
            body: 				"Please enter text",

            reassign: 			"Please select a member",

            receiver: 			"Please select a member",
            subject: 			"Please write a subject",
            message: 			"Plase write a message",

            new_password1:		"Your password must be at least {0} characters long",
            new_password2:{ 
                minlength: 		"Your password must be at least {0} characters long",
                equalTo: 		"Passwords do not match!"
            }
        },

        submitHandler: function(form) {
            form.submit();
        }
	});

	// Validate repy and edit forms
	$('#reply-form, #edit-form').validate();
	
	// Tabs
	if ($('#tabs').length){
		var $location = document.location.toString(),
			$active = $location.split('?show=');
		
		// Setup initial active tab
		$('.tab').hide();

		if ($('#' + $active[1] + '.tab').length)
		{ 
			$('#tabs a[href="#' + $active[1] + '"]').parent('li').addClass('active');
			$('#' + $active[1] + '.tab').show(); 
		} 
		else 
		{ 
			$('#tabs li:first-child').addClass('active');
			$('.tab:first').show(); 
		}

		// Check if validation is needed
		var $form = $('#tabs').parent().attr('id');

		// New board needs validation with tabs
		if ($form == 'new-board'){
			var $validated = $('#new-board').validate({
				onkeyup: false,
				onblur: false
			});

			// Main tabs link
			$('#tabs a').click(function() {
				if ($validated.form()) {
					$('#tabs li').removeClass('active');
					$(this).parent('li').addClass('active');
					$('.tab').hide();
					var $activeTab = $(this).attr('href');
					$($activeTab).show();
				}
			});

			// Inline tab link
			$('.tab-link').click(function(){
				if ($validated.form()) {
					var $activeTab = $(this).attr('href');
					$('#tabs li').removeClass('active');
					$('#tabs a[href="' + $activeTab + '"]').parent('li').addClass('active');
					$('.tab').hide();
					$($activeTab).show();
				}
			});
		}
		// No validation in tabs
		else
		{
			$('#tabs a').click(function(e){
				e.preventDefault();
				
				$('#tabs li').removeClass('active');
				$(this).parent('li').addClass('active');
				$('.tab').hide();
				var $activeTab = $(this).attr('href');
				$($activeTab).show();
			});

			$('.tab-link').click(function(e){
				e.preventDefault();
				
				var $activeTab = $(this).attr('href');
				$('#tabs li').removeClass('active');
				$('#tabs a[href="' + $activeTab + '"]').parent('li').addClass('active');
				$('.tab').hide();
				$($activeTab).show();
			});
		}
	}

	// Nested tabs
	if ($('#nested-tabs').length){
		$('.nested-tab, #nested-tabs-actions .actions-container').hide();
		$('#nested-tabs li:first-child').addClass('active');
		$('.nested-tab:first, #nested-tabs-actions #shtum-invite.actions-container').show(); 

		$('#nested-tabs a').click(function(e){
			e.preventDefault();

			$('#nested-tabs li').removeClass('active');
			$(this).parent('li').addClass('active');
			$('.nested-tab, #nested-tabs-actions .actions-container').hide();

			var $activeTab = $(this).attr('href');
			$($activeTab + ', #nested-tabs-actions ' + $activeTab + '-invite.actions-container').show();
		});
	}

// Some actions are visible only when some option is selected
	
	// Received messages/invites form
	$('form#received .checkbox').click(function(){
		if($('.c-on').parent('li').hasClass('new'))
			$('#mark-as-read.button, #accept-selected-invites.button, #decline-selected-invites.button').toggle();
		
		if($('.c-on').length)
			$('#send-to-trash-received.button').toggle();	
	});

	// Send invities form
	$('form#sent .checkbox').click(function(){
		if($('.c-on').length)
			$('#withdraw-selected-invites.button').toggle();
	});

	// Trash form
	$('form#trash .checkbox').click(function(){
		if($('.c-on').length)
			$('#send-to-trash-trash.button').toggle();	
		else
			$('#empty-trash.button').toggle();
	});

	// Remove memebers
	$('#members-remove').click(function(){
		if($('.c-on').length)
			$('#remove-selected-members.button').toggle();
	});

	// Remove tags tags
	$('#manage-tags').click(function(){
		if($('.c-on').length)
			$('#remove-selected-tags.button').toggle();
	});	

// End of custom actions 


	// Placeholder support for IE
    if($('html').hasClass('ie')) {
    	$('input, textarea').placeholder();
    }
});